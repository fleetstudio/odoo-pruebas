FROM odoo:12
WORKDIR /app
COPY ./entrypoint.sh /
COPY ./odoo.conf /etc/odoo/
EXPOSE 8069 8070 8071
COPY . /app